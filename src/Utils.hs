module Utils
    ( sumMap
    , choose
    , bernoulliNumber
    , factorial
    , fallingFactorial
    ) where

sumMap :: (Num b) => [a] -> (a -> b) -> b
sumMap r f = sum $ map f r

choose :: Integer -> Integer -> Integer
choose n k = product [k + 1 .. n] `div` product [1 .. n - k]

bernoulliNumber :: Integer -> Rational
bernoulliNumber m = sumMap
    [0 .. m]
    (\k -> sumMap
        [0 .. k]
        (\v ->
            (-1)
                ^ v
                * fromInteger (k `choose` v)
                * fromInteger (v + 1)
                ^ m
                / fromInteger (k + 1)
        )
    )

factorial :: Integer -> Integer
factorial n = product [1 .. n]

fallingFactorial :: Integer -> Integer -> Integer
fallingFactorial x n = product [x - n + 1 .. x]
