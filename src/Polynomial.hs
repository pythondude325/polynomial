module Polynomial
    ( Polynomial
    , x
    , fromNum
    , toFunction
    , (*-), (-*)
    , coeffs, powerCoeffs
    )
where

import           Data.Function
import           Data.Functor
import           Data.List
import           Data.Ratio
import Utils

newtype Polynomial k =
  Polynomial [k]

x :: Num k => Polynomial k
x = Polynomial [0, 1]

fromNum :: Num k => k -> Polynomial k
fromNum n = Polynomial [n]

coeffs :: Polynomial k -> [k]
coeffs (Polynomial cs) = cs

powerCoeffs :: Polynomial k -> [(Integer, k)]
powerCoeffs p = zip [0 ..] (coeffs p)

infixl 8 *-
(*-) :: Num k => Polynomial k -> k -> Polynomial k
p *- s = Polynomial $ map (* s) (coeffs p)

infix 8 -*
(-*) :: Num k => k -> Polynomial k -> Polynomial k
(-*) = flip (*-)

infix 8 /-
(/-) :: Fractional k => Polynomial k -> k -> Polynomial k
p /- s = p *- recip s

zipWithZeros :: Num a => [a] -> [a] -> [(a, a)]
zipWithZeros (x : xs) (y : ys) = (x, y) : zipWithZeros xs ys
zipWithZeros []       ys       = zip (repeat 0) ys
zipWithZeros xs       []       = zip xs (repeat 0)

showPower :: String -> Integer -> String
showPower _   0 = ""
showPower var 1 = var
showPower var p = var ++ "^" ++ show p

instance Show k => Show (Polynomial k) where
    show p =
        powerCoeffs p
            & map (\(p, c) -> "(" ++ show c ++ ")" ++ showPower "x" p)
            & reverse
            & intercalate "+"

instance Num k => Num (Polynomial k) where
    fromInteger x = fromNum $ fromInteger x
    p + q = Polynomial $ map (uncurry (+)) $ zipWithZeros (coeffs p) (coeffs q)
    p * q =
        powerCoeffs p
            & map
                  (\(n, c) ->
                      replicate (fromIntegral n) 0 ++ map (* c) (coeffs q)
                  )
            & map Polynomial
            & sum
    negate p = coeffs p & map negate & Polynomial
      -- These are probably wrong
    abs p = coeffs p & map abs & Polynomial
    signum p = coeffs p & map signum & Polynomial

toFunction :: Num k => Polynomial k -> k -> k
toFunction p x = powerCoeffs p & map (\(p, c) -> c * x ^ p) & sum