module Polynomial.Faulhaber
    ( faulhaber
    , polynomialSum
    , nGonNumbers
    )
where

import Data.Function
import Data.Ratio
import Polynomial
import Utils

-- Polynomials are euclidean domains as long as the coefficients are fields (Fractional in haskell)
-- syntheticDivision :: Fractional k => Polynomial k -> Polynomial k -> (Polynomial k, Polynomial k)

-- Given an integer P, the function will return a polynomial Q such that Q(x) is equal to the sum of k^P where k is 1 to x
faulhaber :: Integer -> Polynomial Rational
faulhaber p = (1 % (p + 1)) -* (x ^ (p + 1)) + (1 % 2) -* (x ^ p) + sumMap
    [2 .. p]
    (\k ->
        (bernoulliNumber k / fromInteger (factorial k) * fromInteger
                (fallingFactorial p (k - 1))
            )
            -* (x ^ (p - k + 1))
    )

-- Given a polynomial P of degree m, the function will return a polynomial Q of degree m+1 such that Q(x) is equal to the sum of P(k) where k is 1 to x
polynomialSum :: Polynomial Rational -> Polynomial Rational
polynomialSum p =
    powerCoeffs p
    & map (\(p, c) -> c -* faulhaber p)
    & sum

nGonNumbers :: Rational -> Polynomial Rational
nGonNumbers n =
    polynomialSum ((m - 1) + (m - 2) * (x - 2))
    where m = fromNum n

triangleNumbers :: Polynomial Integer
triangleNumbers = (x * (x - 1) `div` 2)